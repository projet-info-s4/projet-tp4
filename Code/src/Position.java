public class Position {
    private int i;
    private int j;

    private Position(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public static Position of(int i,int j){
        return new Position(i,j);
    }

    public static Position of(Personne personne){
        return new Position(personne.getPosX(),personne.getPosY());
    }
}