import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.exp;
import static java.lang.Math.log;

public class Simulation {


    MTRandom random;
    private int iteration;
    private Personne[] population;
    private Grille repartition;

    private Map<Etat,Integer> nombreEtat;
    private Map<Position,Integer> countCase;


    public Simulation(int iteration, int population,int tailleGrille){
        this.iteration = iteration;
        this.population = new Personne[population];
        this.repartition = new Grille(tailleGrille);
        nombreEtat = new HashMap<>();
        nombreEtat.put(Etat.Susceptible,0);
        nombreEtat.put(Etat.Exposed,0);
        nombreEtat.put(Etat.Infected,0);
        nombreEtat.put(Etat.Recovered,0);
        countCase = new HashMap<>();
        random = new MTRandom();
        random.setSeed(1898756732);
    }

    private double negExp(double inMean)
    {
        return -inMean * log(1 - random.nextRand_0_1());
    }


    public void setPopulation(int nbInfecter){
        if (nbInfecter >= population.length){
            String err =  "Out of bound -> Limit: "+population.length+", Valeur: "+nbInfecter;
            System.err.println(err);
        }
        else {
            int dExpose;
            int dInfecte;
            int dRecover;
            int posX;
            int posY;
            int taille = repartition.getTailleGrille();

            for (int i = 0; i < population.length; i++) {
                dExpose = (int)negExp(3);
                dInfecte = (int)negExp(7);
                dRecover = (int)negExp(365);
                posX = random.nextInt(taille);
                posY = random.nextInt(taille);
                if(i < nbInfecter)
                    population[i] = new Personne(Etat.Infected,dExpose,dInfecte,dRecover);
                else
                    population[i] = new Personne(dExpose,dInfecte,dRecover);
                this.deplacer(posX,posY,population[i]);
                //System.out.println(population[i].toString());
            }
            nombreEtat.put(Etat.Susceptible,population.length-nbInfecter);
            nombreEtat.put(Etat.Infected,nbInfecter);
        }
    }

    public void deplacer(int i,int j,Personne personne){
        int x = personne.getPosX();
        int y = personne.getPosY();
        repartition.getCase(x,y).remove(personne);
        personne.setPos(i,j);
        repartition.getCase(i,j).add(personne);
    }

    public int nbInfecter(int i,int j){
        int sum = 0;
        for(Personne personne : repartition.getCase(i,j)){
            if(personne.estInfecte()){
                sum++;
            }
        }
        return sum;
    }

    public int voisMoore(int i,int j){
        int taille = repartition.getTailleGrille();
        int nbInfect = 0;
        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                int x = (i + dx + taille) % taille; // Gestion des bords toroïdaux
                int y = (j + dy + taille) % taille;
                nbInfect += nbInfecter(x,y);
            }
        }
        return nbInfect;
    }

    public void Infection(Personne personne){
        Integer Ni = countCase.get(Position.of(personne));
        if (Ni == null){
            Ni = voisMoore(personne.getPosX(), personne.getPosY());
            countCase.put(Position.of(personne),Ni);
        }

        double proba = 1-exp(-0.5*Ni);
        if (random.nextRand_0_1()<proba){
            personne.Infecte();
        }
    }

    @Override
    public String toString() {
        return nombreEtat.get(Etat.Susceptible)+","+nombreEtat.get(Etat.Exposed)+","+nombreEtat.get(Etat.Infected)+","+nombreEtat.get(Etat.Recovered);
    }

    public void resetNbEtat() {
        nombreEtat.put(Etat.Susceptible,0);
        nombreEtat.put(Etat.Exposed,0);
        nombreEtat.put(Etat.Infected,0);
        nombreEtat.put(Etat.Recovered,0);
    }

    public void populationInfection(){
        for (Personne personne : this.population){
            if (personne.getEtat() == Etat.Susceptible)
                Infection(personne);
            else
                personne.changeEtat();

            int tmp = nombreEtat.get(personne.getEtat())+1;
            nombreEtat.put(personne.getEtat(),tmp);
        }
    }

    public void populationDeplacement(){
        int taille = repartition.getTailleGrille();
        int posX;
        int posY;
        for (Personne personne : this.population) {
            posX = random.nextInt(taille);
            posY = random.nextInt(taille);
            this.deplacer(posX, posY, personne);
        }
    }

    public void ecritDonne(PrintWriter fichier,int iteration){
        String ligne = iteration+","+this.toString();
        fichier.println(ligne);
    }

    public void run(int nbInfecter,String nomFichier) {
        try (PrintWriter fichier = new PrintWriter(new FileWriter(nomFichier,true))) {
            this.setPopulation(nbInfecter);
            fichier.println("Iteration,Susceptible,Exposed,Infected,Recovered");
            ecritDonne(fichier,0);
            resetNbEtat();

            for (int i = 0; i < this.iteration; i++) {
                populationInfection();
                populationDeplacement();
                ecritDonne(fichier,i+1);
                resetNbEtat();
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }



}
