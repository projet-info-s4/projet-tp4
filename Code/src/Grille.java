//package Code.src;

import java.util.LinkedList;

/*
* L'ideéal serait de créer ici une fonction de hash.
* Et d'attribuer un code de hash à chaque case, du style (300*ligne+colonne).
 */


public class Grille
{
    // Liste de cases contenant la liste des personnes qui y sont.
        /* REMARQUE :
         * Le tableau à deux dimensions n'est pas la structure idéale.
         * Il y a beaucoup trop de cases à parcourir en proportion.
         * L'idéal serait une table de Hash. Ou une liste composée de
         * "maillons" contenant deux valeurs : un nombre représentant la case et la liste chaînée.
         */
    private LinkedList<Personne>[][] grille;
    private int tailleGrille;
    // char[] posEtats = {'S','E','I','R'};

    public Grille(int tailleGrille){
        this.grille = new LinkedList[tailleGrille][tailleGrille];
        for (int i = 0; i < tailleGrille; i++) {
            for (int j = 0; j < tailleGrille; j++) {
                this.grille[i][j] = new LinkedList<>();
            }
        }
        this.tailleGrille = tailleGrille;
    }
    
    public void addGrille(int i,int j,Personne personne) {
        
        if(i >= tailleGrille || j >= tailleGrille || i<0 || j<0) {
            String err =  "Out of bound -> Limit: "+tailleGrille+", Valeur: ["+i+";"+j+"]";
            System.err.println(err);
        }
        else {
            grille[i][j].add(personne);
        }
    }

    public void removeGrille(int i,int j,Personne personne){
        grille[i][j].remove(personne);
    }
    
    public LinkedList<Personne> getCase(int i,int j){
        return grille[i][j];
    }

    public int getTailleGrille() {
        return tailleGrille;
    }

    /*
    private LinkedList<Personne> Population(int nbPers)
    {
        MTRandom rand = new MTRandom();
        int nbInfectes = nbPers / 1000;
        LinkedList<Personne> popu = new LinkedList<Personne>();
        for (int i = 0; i < nbInfectes; i++) {
            int coorX = rand.nextInt(tailleGrille);
            int coorY = rand.nextInt(tailleGrille);
            popu.add(Personne.of('I', 0, coorX, coorY));
        }
        for (int i = nbInfectes; i < nbPers; i++) {
            int coorX = rand.nextInt(tailleGrille);
            int coorY = rand.nextInt(tailleGrille);
            popu.add(Personne.of('S', 0, coorX, coorY));
        }
        this.population = popu;
    }

    private boolean voisinage_Moore()
    {
        for( case : this.Repartition_population)
        {
            int i = ligneCase, int j = colonne;
            for
            Repartition_population[case]

        }
    }

    public static void main(String[] args)
    {
        grille monTest1 /*= grille.of() ;
    }
*/

}

