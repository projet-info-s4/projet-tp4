
import java.util.LinkedList;

import static java.lang.Math.*;

public class Personne extends LinkedList<Personne>
{

    // attributs
    private Etat etat;
    private int temps;
    private final int dE;  
    private final int dI;  
    private final int dR;  
    private int posX;
    private int posY;

    // constructeurs privés

    public Personne(int dE, int dI, int dR)
    {
        this.etat = Etat.Susceptible;
        this.temps = 0;
        this.dE = dE;
        this.dI = dI;
        this.dR = dR;
        posX = 0;
        posY = 0;
    }

    public Personne(Etat etat, int dE, int dI, int dR){
        this.etat = etat;
        this.temps = 0;
        this.dE = dE;
        this.dI = dI;
        this.dR = dR;
        posX = 0;
        posY = 0;
    }

    // La méthode permet d'incrémenter le temps passé 
    //(par la personne) dans un état.
    private void setTemps()
    {
        this.temps += 1;
    }

    private void resetTemps()
    {
        this.temps = 0;
    }

    public boolean estInfecte()
    {
        return (this.etat == Etat.Infected);
    }

    // Calcule la probabilité pour un individu 
    //susceptible de devenir infecté (passage de S à E)
    protected double probaInfection(int nbInfectesVoisinage)
    {
        double proba = 1- exp((-0.5) * nbInfectesVoisinage);
        return (proba);
    }

    public void Infecte(){
        if(this.etat == Etat.Susceptible) {
            this.setEtat(Etat.Exposed);
            this.resetTemps();
        }
    }
    public boolean changeEtat()
    {
        if (etat != Etat.Susceptible) {
            this.setTemps();
            switch (etat) {
                case Exposed:
                    // on a été exposé trop longtemps → contaminé.
                    if (this.temps > this.dE) {
                        this.setEtat(Etat.Infected);
                        this.resetTemps();
                    }
                    break;
                case Infected:
                    // on a été infecté assez longtemps → on est immunisé.
                    if (this.temps > this.dI) {
                        this.setEtat(Etat.Recovered);
                        this.resetTemps();
                    }
                    break;
                case Recovered:
                    // on n'est plus immunisé au bout d'un certain temps.
                    if (this.temps > this.dR) {
                        this.setEtat(Etat.Susceptible);
                        this.resetTemps();
                    }
                    break;
            }
        }

        // si on vient de changer d'état le temps vaut 0,
        // sinon on est resté dans le même état, temps minimum : 1.
        return (this.temps == 0) ;
    }

    public Etat getEtat(){
        return etat;
    }

    // La méthode permet de changer l'état dans lequel la personne se trouve.
    // Nouvel état fourni en argument de la fonction.
    public void setEtat(Etat etat)
    {
        this.etat = etat;
    }

    public void setPos(int nvAbs, int nvOrd)
    {
        this.posX = nvAbs;
        this.posY = nvOrd;
    }

    // La méthode permet de connaître les coordonnées d'une personne.
    public int getPosX()
    {
        return posX;
    }

    public int getPosY()
    {
        return posY;
    }

    // La méthode permet de connaître les coordonnées d'une personne.
    public String seTrouveEn()
    {
        return String.format("[%d ; %d]",posX,posY);
    }

    public String toString()
    {
        return String.format
                ( "La personne se trouve dans l'état : %s;%nTemps passé 
                dans cet état : %d jour(s);%nPosition sur la grille : (%d %d)",
                        this.etat.toString(), this.temps, this.posX,this.posY);
    }
}
